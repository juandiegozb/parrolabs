<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    use HasFactory;

    protected $primaryKey = 'DEPARTMENT_ID';

    public function company()
    {
        return $this->belongsTo(Company::class, 'COMPANY_ID');
    }

    public function positions()
    {
        return $this->hasMany(Position::class, 'DEPARTMENT_ID');
    }
}
