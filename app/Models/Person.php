<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Person extends Model
{
    use HasFactory;

    protected $table = 'person';

    protected $fillable = [
        'user_id',
        'name',
        'lastname',
        'birthdate',
        'education_id',
        'salary',
        'experience_years'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
