<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MaxPositionLimit extends Model
{
    use HasFactory;

    protected $primaryKey = 'LIMIT_ID';

    public function company()
    {
        return $this->belongsTo(Company::class, 'COMPANY_ID');
    }
}
