<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Offer extends Model
{
    use HasFactory;
    protected $primaryKey = 'id';

    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id', 'COMPANY_ID');
    }

    public function users()
    {
        return $this->belongsToMany(User::class, 'offer_user', 'offer_id', 'user_id')
            ->withPivot('company_id');
    }

    public function maxPositionLimit()
    {
        return $this->hasOne(MaxPositionLimit::class, 'company_id', 'company_id');
    }

    public function offerUsers()
    {
        return $this->hasMany(OfferUser::class);
    }

}
