<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    use HasFactory;

    protected $primaryKey = 'COMPANY_ID';

    public function users()
    {
        return $this->hasMany(User::class, 'user_id');
    }

    public function offers()
    {
        return $this->hasMany(Offer::class, 'company_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'USER_ID', 'id');
    }

    public function offerUsers()
    {
        return $this->hasMany(OfferUser::class);
    }

}
