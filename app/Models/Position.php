<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Position extends Model
{
    use HasFactory;

    protected $primaryKey = 'POSITION_ID';

    public function department()
    {
        return $this->belongsTo(Department::class, 'DEPARTMENT_ID');
    }

    public function terms()
    {
        return $this->hasMany(Term::class, 'POSITION_ID');
    }
}
