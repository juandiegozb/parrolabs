<?php

namespace App\Traits;

trait RedirectBasedOnUserType
{
    protected function redirectBasedOnUserType($user)
    {
        return redirect()->route($user->type_user == 1 ? 'enterprise.home' : 'candidate.home');
    }
}
