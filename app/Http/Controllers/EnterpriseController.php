<?php

namespace App\Http\Controllers;

use App\Models\Company;
use App\Models\MaxPositionLimit;
use App\Models\Offer;
use App\Models\Person;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class EnterpriseController extends Controller
{

    /**
     * Method in charge of displaying the main page of the site, it is in charge of redirecting whether it has a
     * company associated with the user who logs in.
     */
    public function showHomeCompany() {
        $user = Auth::user();
        $employer = Company::where('user_id', $user->id)->first();

        if ($employer) {
            return view('enterprise.home');
        }else {
            return view('enterprise.create');
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     *
     *  In charge of storing the form to save the company
     *
     */
    public function createCompany(Request $request){

        $validator = Validator::make($request->all(), [
            'company_name' => 'required|string|max:255',
            'company_address' => 'required|string|max:255',
            'zip_code' => 'required|numeric',
        ], [
            'required' => 'Hey, the :attribute field is required.',
            'string' => 'Oops, the :attribute field must be a string.',
            'max' => 'Uh-oh, the :attribute field cannot exceed :max characters.',
            'numeric' => 'Oops, the :attribute field must be a number.',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $company = new Company();
        $user = Auth::user();

        $company->USER_ID = $user->id;
        $company->COMPANY_NAME = $request->company_name;
        $company->COMPANY_ADDRESS = $request->company_address;
        $company->ZIP_CODE = $request->zip_code;

        $company->save();

        $company_id = Company::where('user_id', $user->id)->first()->COMPANY_ID;

        $maxLimits = new MaxPositionLimit();
        $maxLimits->COMPANY_ID = $company_id;
        $maxLimits->MAXIMUM_NUMBER = rand(1,10);
        $maxLimits->POSITIONS_APPLIED = 0;
        $maxLimits->PUBLISHED_JOBS = 0;

        $maximum = $maxLimits->MAXIMUM_NUMBER;

        $maxLimits->save();


        session()->flash('success', 'Amazing! Now you can have control of your published jobs and candidates to fill them. your maximum number of publications is: '. $maximum);
        return redirect()->route('enterprise.home')->withInput();
    }


    /**
     *  Create job offers, which will be viewed by a candidate user.
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function createJobOffer(Request $request) {

        $validator = Validator::make($request->all(), [
            'offer' => 'required',
            'role' => 'required',
            'salary' => 'required|numeric',
            'year_experience' => 'required|numeric',
        ], [
            'offer.required' => 'The offer field is required.',
            'role.required' => 'The role field is required.',
            'salary.required' => 'The salary field is required.',
            'salary.numeric' => 'The salary must be a numeric value.',
            'year_experience.required' => 'The years of experience field is required.',
            'year_experience.numeric' => 'The years of experience must be a numeric value.',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $user = Auth::user();
        $company_id = Company::where('user_id', $user->id)->first()->COMPANY_ID;

        $maxPositionLimit = MaxPositionLimit::where('COMPANY_ID', $company_id)->first();

        if ($maxPositionLimit) {
            $maximumNumber = $maxPositionLimit->MAXIMUM_NUMBER;
            $offerCount = Offer::where('COMPANY_ID', $company_id)->count();

            if ($offerCount >= $maximumNumber) {
                session()->flash('error', 'You have exceeded the maximum number of publications allowed.');
                return redirect()->route('enterprise.home')->withInput();
            }
        }

        $offer = new Offer;
        $offer->company_id = $company_id;
        $offer->offer_id = Offer::max('id') + 1;
        $offer->status = 1;
        $offer->offer = $request->offer;
        $offer->role = $request->role;
        $offer->salary = $request->salary;
        $offer->year_experience = $request->year_experience;

        $offer->save();

        MaxPositionLimit::where('COMPANY_ID', $offer->company_id)
            ->increment('PUBLISHED_JOBS');

        session()->flash('success', 'You have successfully created your job offer.');
        return redirect()->route('enterprise.home')->withInput();

    }

    /**
     *  Displays a list of jobs that have applied to your offers
     */
    public function showApplicants() {
        if (!Auth::check()) {
            return json_encode(["error" => "User is not authenticated."]);
        }

        $user = Auth::user();
        $employer = Company::where('user_id', $user->id)->first();

        $companyId = $employer->COMPANY_ID;

        $appliedPersons = Person::join('users', 'person.user_id', '=', 'users.id')
            ->join('offer_user', 'users.id', '=', 'offer_user.user_id')
            ->join('offers', 'offer_user.offer_id', '=', 'offers.id')
            ->where('offer_user.company_id', $companyId)
            ->select('person.*', 'offers.OFFER')
            ->get();

        return view('enterprise.applicants', ['appliedPersons' => $appliedPersons]);
    }
}
