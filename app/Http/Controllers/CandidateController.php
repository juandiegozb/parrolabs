<?php

namespace App\Http\Controllers;

use App\Models\Company;
use App\Models\MaxPositionLimit;
use App\Models\Offer;
use App\Models\Person;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CandidateController extends Controller
{
    /**
     *  Displays the candidate's home page, always checking that he/she is logged in and has access permissions
     */
    public function showHomeCandidate(){

        $user = Auth::user();
        $person = Person::where('user_id', $user->id)->first();

        // find available jobs
        $openOffers = Offer::where('status', 1)
            ->with('company')
            ->get();

        if ($person) {
            return view('candidate.home', ['openOffers' => $openOffers]);
        }else {
            return view('candidate.create');
        }
    }

    /**
     * Responsible for associating a candidate with a user.
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    Public function createPersonal(Request $request){
        $messages = [
            'name.required' => 'Please enter your name.',
            'name.string' => 'The name field must be a string.',
            'lastname.required' => 'Please enter your last name.',
            'lastname.string' => 'The last name field must be a string.',
            'birthdate.required' => 'Please enter your birthdate.',
            'birthdate.date' => 'The birthdate field must be a valid date.',
            'salary.required' => 'Please enter your salary range.',
            'salary.numeric' => 'The salary range field must be a number.',
            'experience.required' => 'Please enter your years of experience.',
            'experience.numeric' => 'The years of experience field must be a number.',
            'studies.required' => 'Please select your highest level of education.'
        ];


        $request->validate([
            'name' => 'required|string',
            'lastname' => 'required|string',
            'birthdate' => 'required|date',
            'salary' => 'required|numeric',
            'experience' => 'required|numeric',
            'studies' => 'required'
        ], $messages);

        $person = new Person();
        $user = Auth::user();

        $person->user_id = $user->id;
        $person->name = $request->name;
        $person->lastname = $request->lastname;
        $person->birthdate = $request->birthdate;
        $person->education_id = $request->studies;
        $person->salary = $request->salary;
        $person->experience_years = $request->experience;

        $person->save();

        session()->flash('success', 'Incredible! Now we have a clearer picture of who you are');
        return redirect()->route('candidate.home')->withInput();

    }

    /**
     * Apply for an offer :)
     * @param $id
     */
    public function applyOffer($id) {

        $user = auth()->user();

        /*if ($user->offers()->where('offers.offer_id', $id)->exists()) {
            return response()->json(['status' => 'already applied']);
        }*/



        $offer = Offer::findOrFail($id);

        $user->offers()->attach($offer, [
            'company_id' => $offer->company_id,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        MaxPositionLimit::where('COMPANY_ID', $offer->company_id)
            ->increment('POSITIONS_APPLIED');

        session()->flash('success', 'Great, you applied for an offer, good luck!');

        return redirect(route('candidate.home'))->withInput();
    }
}
