<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Traits\RedirectBasedOnUserType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class RegisterController extends Controller
{
    use RedirectBasedOnUserType;

    public function showRegistrationForm() {

        if (auth()->check()) {
            $user = auth()->user();
            return $this->redirectBasedOnUserType($user);
        }

        return view('auth.register');
    }

    public function register(Request $request) {
        $messages = [
            'email.required' => "Oops! Don't forget to enter your email address.",
            'email.email' => "Uh-oh! That doesn't look like a valid email address.",
            'password.required' => "Hold on! You need to enter your password.",
            'confirm_password.required' => "Wait a minute! Don't forget to confirm your password.",
            'confirm_password.same' => "Oops! The password confirmation doesn't match.",
            'user_type.required' => "Hey there! Please select a user type.",
        ];


        $request->validate([
            'email' => 'required|email|unique:users,email',
            'password' => 'required',
            'confirm_password' => ['required', 'same:password'],
            'user_type' => 'required|in:1,2',
        ], $messages);

        $user = new User;

        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->type_user = $request->user_type;

        $user->save();

        session()->flash('success', 'Great! you created your user');
        return redirect()->route('login')->withInput();

    }
}
