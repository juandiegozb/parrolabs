<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Traits\RedirectBasedOnUserType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    use RedirectBasedOnUserType;

    public function showLoginForm()
    {
        if (auth()->check()) {
            $user = auth()->user();
            return $this->redirectBasedOnUserType($user);
        }

        return view('auth.login');
    }

    public function login(Request $request)
    {
        $messages = [
            'email.required' => "Oops! The email field is required.",
            'email.email' => "Uh-oh! The email format is invalid.",
            'password.required' => "Oops! The password field is required.",
        ];

        $credentials = $request->validate([
            'email' => 'required|email',
            'password' => 'required'
        ], $messages);

        if (auth()->attempt($credentials)) {
            $user = auth()->user();
            return redirect()->route($user->type_user == 1 ? 'enterprise.home' : 'candidate.home');
        }

        session()->flash('error', 'Oops! It seems that the information you have typed is not correct. Please try again.');
        return redirect()->route('login')->withInput();

    }

    public function logout()
    {
        Auth::logout();
        session()->flash('success', 'So long, digital adventurer! You have successfully disconnected.');
        return redirect()->route('login')->withInput();
    }
}
