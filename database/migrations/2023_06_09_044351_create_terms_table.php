<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('terms', function (Blueprint $table) {
            $table->bigIncrements('TERM_ID');
            $table->foreignId('POSITION_ID')->constrained('positions', 'POSITION_ID');
            $table->string('ROLE', 150);
            $table->integer('REQUIRED_EXPERIENCE_YEARS');
            $table->integer('SALARY');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('terms');
    }
};
