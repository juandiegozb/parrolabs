<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('users', function (Blueprint $table) {
            $table->unsignedBigInteger('type_user')->nullable()->after('password');
            $table->index('type_user');
            $table->foreign('type_user')->references('USER_TYPE_ID')->on('user_type')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropForeign(['type_user']);
            $table->dropIndex(['type_user']);
            $table->dropColumn('type_user');
        });
    }
};
