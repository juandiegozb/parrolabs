<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('departments', function (Blueprint $table) {
            $table->bigIncrements('DEPARTMENT_ID');
            $table->foreignId('COMPANY_ID')->constrained('companies', 'COMPANY_ID');
            $table->string('DEPARTMENT_NAME', 120);
            $table->text('DEPARTMENT_DESCRIPTION');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('departments');
    }
};
