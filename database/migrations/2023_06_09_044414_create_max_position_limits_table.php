<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('max_position_limits', function (Blueprint $table) {
            $table->bigIncrements('LIMIT_ID');
            $table->foreignId('COMPANY_ID')->constrained('companies', 'COMPANY_ID');
            $table->integer('MAXIMUM_NUMBER');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('max_position_limits');
    }
};
