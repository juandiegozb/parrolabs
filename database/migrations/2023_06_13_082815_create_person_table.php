<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('person', function (Blueprint $table) {
            $table->bigIncrements('PERSON_ID');
            $table->unsignedBigInteger('USER_ID')->index();
            $table->string('NAME');
            $table->string('LASTNAME');
            $table->date('BIRTHDATE');
            $table->unsignedBigInteger('EDUCATION_ID')->index();
            $table->foreign('USER_ID')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('EDUCATION_ID')->references('EDUCATION_ID')->on('education_list')->onDelete('cascade');
            $table->integer('SALARY');
            $table->integer('EXPERIENCE_YEARS');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('person');
    }
};
