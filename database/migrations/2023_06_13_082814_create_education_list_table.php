<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('education_list', function (Blueprint $table) {
            $table->bigIncrements('EDUCATION_ID');
            $table->string('EDUCATION');
            $table->timestamps();
        });

        DB::table('education_list')->insert([
            ['EDUCATION_ID' => 1, 'EDUCATION' => "Bachelor's degree", 'created_at' => now(), 'updated_at' => now()],
            ['EDUCATION_ID' => 2, 'EDUCATION' => "Master's degree", 'created_at' => now(), 'updated_at' => now()],
            ['EDUCATION_ID' => 3, 'EDUCATION' => 'Doctorate', 'created_at' => now(), 'updated_at' => now()],
            ['EDUCATION_ID' => 4, 'EDUCATION' => 'Diploma', 'created_at' => now(), 'updated_at' => now()],
            ['EDUCATION_ID' => 5, 'EDUCATION' => 'Course', 'created_at' => now(), 'updated_at' => now()],
        ]);
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('education_list');
    }
};
