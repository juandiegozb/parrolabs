<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('positions', function (Blueprint $table) {
            $table->bigIncrements('POSITION_ID');
            $table->foreignId('COMPANY_ID')->constrained('companies', 'COMPANY_ID');
            $table->foreignId('DEPARTMENT_ID')->constrained('departments', 'DEPARTMENT_ID');
            $table->text('POSITION_DESCRIPTION');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('positions');
    }
};
