<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('max_position_limits', function (Blueprint $table) {
            $table->string('PUBLISHED_JOBS')->after('POSITIONS_APPLIED');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('max_position_limits', function (Blueprint $table) {
            $table->dropColumn('PUBLISHED_JOBS');
        });
    }
};
