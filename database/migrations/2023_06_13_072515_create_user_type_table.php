<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('user_type', function (Blueprint $table) {
            $table->bigIncrements('user_type_id');
            $table->string('type', 150);
            $table->timestamps();
        });

        DB::table('user_type')->insert([
            ['user_type_id' => 1, 'type' => 'Employer', 'created_at' => now(), 'updated_at' => now()],
            ['user_type_id' => 2, 'type' => 'Candidate', 'created_at' => now(), 'updated_at' => now()],
        ]);
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('user_type');
    }
};
