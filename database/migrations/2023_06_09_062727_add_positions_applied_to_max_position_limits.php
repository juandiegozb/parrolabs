<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('max_position_limits', function (Blueprint $table) {
            $table->integer('POSITIONS_APPLIED')->default(0)->after('MAXIMUM_NUMBER');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('max_position_limits', function (Blueprint $table) {
            $table->dropColumn('POSITIONS_APPLIED');
        });
    }
};
