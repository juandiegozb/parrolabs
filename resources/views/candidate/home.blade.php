<x-app title="Personal">
    <x-partials.navbar-parrolabs></x-partials.navbar-parrolabs>
    <x-candidate.home :openOffers="$openOffers"></x-candidate.home>
    <x-partials.footer-parrolabs></x-partials.footer-parrolabs>
</x-app>
