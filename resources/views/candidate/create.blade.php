<x-app title="Complete Your Perfil">
    <x-partials.navbar-parrolabs></x-partials.navbar-parrolabs>
    <x-candidate.create_profile></x-candidate.create_profile>
    <x-partials.footer-parrolabs></x-partials.footer-parrolabs>
</x-app>
