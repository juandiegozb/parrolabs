<x-app title="login">
    <x-partials.navbar-parrolabs></x-partials.navbar-parrolabs>
    <x-auth.login></x-auth.login>
    <x-partials.footer-parrolabs></x-partials.footer-parrolabs>
</x-app>
