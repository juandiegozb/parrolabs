<x-app title="register">
    <x-partials.navbar-parrolabs></x-partials.navbar-parrolabs>
    <x-auth.register></x-auth.register>
    <x-partials.footer-parrolabs></x-partials.footer-parrolabs>
</x-app>

