<br><br><br>
<div class="flex w-full p-4 mt-16 text-gray-900 flex justify-center">
    <div class="max-w-screen-xl m-0 sm:m-10 bg-white shadow sm:rounded-lg flex justify-center flex-1">
        <div class="lg:w-1/2 xl:w-5/12 p-6 sm:p-12">
            <div class="mt-12 flex flex-col items-center">
                <h1 class="text-2xl xl:text-3xl font-extrabold">Time to create a new job offer, great!</h1>
                <div class="w-full flex-1 mt-8">
                    @if(session('error'))
                        <div class="bg-red-500 text-center text-white p-4 mb-4">
                            {{ session('error') }}
                        </div>
                    @endif

                    @if(session('success'))
                        <div class="bg-green-500 text-center text-white p-4 mb-4">
                            {{ session('success') }}
                        </div>
                    @endif

                    <div class="my-12 border-b text-center">
                        <div class="leading-none px-2 inline-block text-sm text-gray-600 tracking-wide font-medium bg-white transform translate-y-1/2">
                            New opportunities
                        </div>
                    </div>

                    <div class="mx-auto max-w-xs">
                        <form method="POST" action="{{route('create-job-offer')}}">
                            @csrf

                            <input class="w-full px-8 py-4 rounded-lg font-medium bg-gray-100 border border-gray-200 placeholder-gray-500 text-sm focus:outline-none focus:border-gray-400 focus:bg-white" type="text" name="offer"  placeholder="Offer"/>
                            @error('offer')
                                <span class="text-red-500 text-sm">{{ $message }}</span>
                            @enderror
                            <br><br>

                            <input class="w-full px-8 py-4 rounded-lg font-medium bg-gray-100 border border-gray-200 placeholder-gray-500 text-sm focus:outline-none focus:border-gray-400 focus:bg-white" type="text" name="role"  placeholder="Role"/>
                            @error('role')
                                <span class="text-red-500 text-sm">{{ $message }}</span>
                            @enderror
                            <br><br>

                            <input class="w-full px-8 py-4 rounded-lg font-medium bg-gray-100 border border-gray-200 placeholder-gray-500 text-sm focus:outline-none focus:border-gray-400 focus:bg-white" type="number" name="salary"  placeholder="Salary"/>
                            @error('salary')
                                <span class="text-red-500 text-sm">{{ $message }}</span>
                            @enderror
                            <br><br>

                            <input class="w-full px-8 py-4 rounded-lg font-medium bg-gray-100 border border-gray-200 placeholder-gray-500 text-sm focus:outline-none focus:border-gray-400 focus:bg-white" type="number" name="year_experience"  placeholder="Years of Experience"/>
                            @error('year_experience')
                                <span class="text-red-500 text-sm">{{ $message }}</span>
                            @enderror

                            <button type="submit" class="mt-5 tracking-wide font-semibold bg-indigo-500 text-gray-100 w-full py-4 rounded-lg hover:bg-indigo-700 transition-all duration-300 ease-in-out flex items-center justify-center focus:shadow-outline focus:outline-none">
                                <span class="ml-3"> Publish a new Opportunity! </span>
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="flex-1 bg-indigo-100 text-center hidden lg:flex">
            <div class="m-12 xl:m-16 w-full bg-contain bg-center bg-no-repeat" style="background-image: url( {{ asset('/img/job_offers.svg')  }} );"></div>
        </div>
    </div>
</div>
