<div class="flex w-full p-4 mt-16 text-gray-900 flex justify-center">
    <div class="max-w-screen-xl m-0 sm:m-10 bg-white shadow sm:rounded-lg flex justify-center flex-1">
        <div class="lg:w-1/2 xl:w-5/12 p-6 sm:p-12">
            <div class="mt-12 flex flex-col items-center">
                <h1 class="text-2xl xl:text-3xl font-extrabold">Find your ideal job and give your career a boost - register now!</h1>
                <div class="w-full flex-1 mt-8">
                    <div class="my-12 border-b text-center">
                        <div
                            class="leading-none px-2 inline-block text-sm text-gray-600 tracking-wide font-medium bg-white transform translate-y-1/2">
                            Create an account and discover a world of opportunities.
                        </div>
                    </div>

                    <div class="mx-auto max-w-xs">
                        <form method="POST">
                            @csrf

                            <input class="w-full px-8 py-4 rounded-lg font-medium bg-gray-100 border border-gray-200 placeholder-gray-500 text-sm focus:outline-none focus:border-gray-400 focus:bg-white" type="email" name="email"  placeholder="Email"/>
                            @error('email')
                                <span class="text-red-500 text-sm">{{ $message }}</span>
                            @enderror

                            <input class="w-full px-8 py-4 rounded-lg font-medium bg-gray-100 border border-gray-200 placeholder-gray-500 text-sm focus:outline-none focus:border-gray-400 focus:bg-white mt-5" type="password"  name="password" placeholder="Password"/>
                            @error('password')
                                <span class="text-red-500 text-sm">{{ $message }}</span>
                            @enderror

                            <input class="w-full px-8 py-4 rounded-lg font-medium bg-gray-100 border border-gray-200 placeholder-gray-500 text-sm focus:outline-none focus:border-gray-400 focus:bg-white mt-5" type="password"  name="confirm_password" placeholder="Confirm Password"/>
                            @error('confirm_password')
                            <span class="text-red-500 text-sm">{{ $message }}</span>
                            @enderror
                            <br><br>
                            <select class="w-full px-8 py-4 rounded-lg font-medium bg-gray-100 border border-gray-200 placeholder-gray-500 text-sm focus:outline-none focus:border-gray-400 focus:bg-white" name="user_type">
                                <option>Select an option</option>
                                <option value="1">Employer</option>
                                <option value="2">Candidate</option>
                            </select>
                            @error('country')
                            <span class="text-red-500 text-sm">{{ $message }}</span>
                            @enderror


                            <button type="submit" class="mt-5 tracking-wide font-semibold bg-indigo-500 text-gray-100 w-full py-4 rounded-lg hover:bg-indigo-700 transition-all duration-300 ease-in-out flex items-center justify-center focus:shadow-outline focus:outline-none">
                                <span class="ml-3"> Sign Up</span>
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="flex-1 bg-indigo-100 text-center hidden lg:flex">
            <div class="m-12 xl:m-16 w-full bg-contain bg-center bg-no-repeat" style="background-image: url( {{ asset('/img/register.svg')  }} );">
            </div>
        </div>
    </div>
