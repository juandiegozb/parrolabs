<nav class="bg-white dark:bg-gray-900 fixed w-full z-20 top-0 left-0 border-b border-gray-200 dark:border-gray-600">
    <div class="max-w-screen-xl flex flex-wrap items-center justify-between mx-auto p-4">
        <a href="{{ route('main') }}" class="flex items-center">
            <img src="{{ asset('/img/parrolabs.png') }}" class="h-6 mr-3" alt="parrolabs Logo">
            <span class="self-center text-2xl font-mono whitespace-nowrap dark:text-white">Jobs</span>
        </a>

        <div class="hidden sm:ml-6 sm:block">
            @auth
            <div class="flex space-x-4">
                @if(auth()->user()->type_user == 1)
                    <a href="{{ route('enterprise.seeApplicants') }}" class="inline-block px-4 py-2 bg-blue-500 hover:bg-blue-600 text-white font-semibold rounded-lg shadow-md">See Applicants</a>
                @endif
            </div>
            @endauth
        </div>

        <div class="flex md:order-2">
            @auth
                <span class="px-4 py-2 text-center font-extrabold mr-3 md:mr-0 text-gray-900 rounded hover:bg-gray-100 md:hover:bg-transparent md:border-0 md:hover:text-blue-700 md:p-0 dark:text-white md:dark:hover:text-blue-500 dark:hover:bg-gray-700 dark:hover:text-white md:dark:hover:bg-transparent flex items-center">
                    @if(auth()->user()->type_user == 1)
                        {{ auth()->user()->company->COMPANY_NAME ?? 'Guest' }}
                    @elseif(auth()->user()->type_user == 2)
                        {{ auth()->user()->person->NAME ?? 'Guest' }}
                    @else
                        Guest
                    @endif
                </span>

                <form action="{{ route('logout') }}" method="post">
                    @csrf
                    <button type="submit" class="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-4 py-2 text-center mr-3 md:mr-0 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800 ml-2">Power down</button>
                </form>
            @endauth

                @guest
                    @if(Request::is('register'))
                        <a href="{{ route('login') }}" class="text-white bg-blue-500 hover:bg-blue-600 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-4 py-2 text-center md:mr-0 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800 ml-2">Login</a>
                    @else
                        <a href="{{ route('register') }}" class="text-white bg-green-500 hover:bg-green-600 focus:ring-4 focus:outline-none focus:ring-green-300 font-medium rounded-lg text-sm px-4 py-2 text-center md:mr-0 dark:bg-green-600 dark:hover:bg-green-700 dark:focus:ring-green-800 ml-2">Sign Up</a>
                    @endif
                @endguest


            <button data-collapse-toggle="navbar-sticky" type="button" class="inline-flex items-center p-2 text-sm text-gray-500 rounded-lg md:hidden hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-gray-200 dark:text-gray-400 dark:hover:bg-gray-700 dark:focus:ring-gray-600" aria-controls="navbar-sticky" aria-expanded="false">
                <span class="sr-only">Open main menu</span>
                <svg class="w-6 h-6" aria-hidden="true" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M3 5a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 15a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z" clip-rule="evenodd"></path></svg>
            </button>
        </div>
    </div>
</nav>
