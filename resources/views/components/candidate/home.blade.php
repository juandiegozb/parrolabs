<div class="flex flex-col items-center justify-center h-screen">
    <div class="container justify-center overflow-y-auto max-h-[80vh] w-full mt-16 p-4 text-center bg-white border border-gray-200 rounded-lg shadow sm:p-8 dark:bg-gray-800 dark:border-gray-700">
        @if(session('success'))
            <div class="bg-green-500 text-center text-white p-4 mb-4">
                {{ session('success') }}
            </div>
        @endif
        <h5 class="mb-2 text-3xl font-bold text-gray-900 dark:text-white">Discover Your Dream Job in the Following List</h5>
        <br>
        <p class="mb-5 font-mono text-base text-gray-500 sm:text-lg dark:text-gray-400">Unleash your potential and embark on an exciting journey to find your dream job. Explore the curated collection below, crafted exclusively for individuals like you who dare to dream big. Your perfect career opportunity awaits!</p>
        <hr>

        <div class="relative overflow-x-auto shadow-md sm:rounded-lg">
            <div class="pb-4 bg-white dark:bg-gray-900">
            </div>
            <table class="w-full text-sm text-left text-gray-500 dark:text-gray-400">
                <thead class="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
                <tr>
                    <th scope="col" class="px-6 py-3">
                        Title Position
                    </th>
                    <th scope="col" class="px-6 py-3">
                        Company
                    </th>
                    <th scope="col" class="px-6 py-3">
                        Role
                    </th>
                    <th scope="col" class="px-6 py-3">
                        Required Experience
                    </th>
                    <th scope="col" class="px-6 py-3">
                        Salary
                    </th>

                    <th scope="col" class="px-6 py-3">
                        Ready to Make Moves?
                    </th>
                </tr>
                </thead>
                <tbody>
                @if($openOffers->count() > 0)

                        @foreach($openOffers as $offer)
                            <tr class="bg-white border-b dark:bg-gray-800 dark:border-gray-700 hover:bg-gray-50 dark:hover:bg-gray-600">
                                <th scope="row" class="px-6 py-4 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                                    {{$offer->offer}}
                                </th>
                                <td class="px-6 py-4">
                                    {{$offer->company->COMPANY_NAME}}
                                </td>
                                <td class="px-6 py-4">
                                    {{$offer->role}}
                                </td>
                                <td class="px-6 py-4">
                                    {{$offer->year_experience}}
                                </td>
                                <td class="px-6 py-4">
                                    USD {{$offer->salary}} per year
                                </td>
                                <td class="px-6 py-4">
                                    @if($offer->salary < auth()->user()->person->SALARY)
                                        <span class="font-extrabold  text-red-500">The salary is below your minimum wage.</span>
                                    @elseif($offer->maxPositionLimit->POSITIONS_APPLIED >= $offer->maxPositionLimit->MAXIMUM_NUMBER)
                                        <span class="text-gray-500">The limit of applications for this company has been exceeded.</span>
                                    @elseif($offer->users->contains(auth()->user()->id))
                                        <span class="font-extrabold text-green-600">Applied</span>
                                    @else
                                        <a href="{{ route('candidate.apply', ['id' => $offer->id]) }}" class="font-medium text-blue-600 dark:text-blue-500 hover:underline">Apply for this offer</a>
                                    @endif
                                </td>
                            </tr>
                        @endforeach

                @else
                    <p>No open offers available.</p>
                @endif

                </tbody>
            </table>
        </div>


    </div>
</div>
