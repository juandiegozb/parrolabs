<x-app title="Register your company">
    <x-partials.navbar-parrolabs></x-partials.navbar-parrolabs>
    <x-enterprice.create_profile></x-enterprice.create_profile>
    <x-partials.footer-parrolabs></x-partials.footer-parrolabs>
</x-app>
