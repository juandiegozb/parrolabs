<x-app title="History Applicants">
    <x-partials.navbar-parrolabs></x-partials.navbar-parrolabs>
    <br><br><br><br>
    <div class="relative overflow-x-auto shadow-md sm:rounded-lg">
        <div class="pb-4 bg-white dark:bg-gray-900"></div>
    <table class="w-full text-sm text-left text-gray-500 dark:text-gray-400">
        <thead class="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
        <tr>
            <th scope="col" class="px-6 py-3">
                Name
            </th>
            <th scope="col" class="px-6 py-3">
                LastName
            </th>
            <th scope="col" class="px-6 py-3">
                Age
            </th>
            <th scope="col" class="px-6 py-3">
                Salary
            </th>
            <th scope="col" class="px-6 py-3">
                Experience
            </th>
            <th scope="col" class="px-6 py-3">
                Published offer
            </th>

            <!--<th scope="col" class="px-6 py-3">
                do you hire it?
            </th>-->
        </tr>
        </thead>
        <tbody>
        @if($appliedPersons->count() > 0)

            @foreach($appliedPersons as $person)
                <tr class="bg-white border-b dark:bg-gray-800 dark:border-gray-700 hover:bg-gray-50 dark:hover:bg-gray-600">
                    <th scope="row" class="px-6 py-4 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                        {{$person->NAME}}
                    </th>
                    <td class="px-6 py-4">
                        {{$person->LASTNAME}}
                    </td>
                    <td class="px-6 py-4">
                        {{$person->BIRTHDATE}}
                    </td>
                    <td class="px-6 py-4">
                        USD  {{$person->SALARY}} Per Year
                    </td>
                    <td class="px-6 py-4">
                        {{$person->EXPERIENCE_YEARS}}
                    </td>
                    <td class="px-6 py-4">
                        {{$person->OFFER}}
                    </td>

                    <!--<td class="px-6 py-4">
                        <a href="#" class="font-medium text-blue-600 dark:text-blue-500 hover:underline">Hire</a>
                    </td>-->
                </tr>
            @endforeach

        @else
            <p>There are no applications at this moment.</p>
        @endif

        </tbody>
    </table>
    </div>
    <x-partials.footer-parrolabs></x-partials.footer-parrolabs>
</x-app>
