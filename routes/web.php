<?php

use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\CandidateController;
use App\Http\Controllers\EnterpriseController;
use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

// Login and Register Routes
Route::get('/login', [LoginController::class, 'showLoginForm'])->name('login');
Route::post('/login', [LoginController::class, 'login']);
Route::get('/register', [RegisterController::class, 'showRegistrationForm'])->name('register');
Route::post('/register', [RegisterController::class, 'register']);
Route::post('/logout', [LoginController::class, 'logout'])->name('logout');





Route::group(['middleware' => 'enterprise.candidate.error'], function (){

    // Main Route
    Route::get('/', function () {
        if (auth()->check()) {
            return redirect()->route(auth()->user()->type_user == 1 ? 'enterprise.home' : 'candidate.home');
        } else {
            return redirect()->route('login');
        }
    })->name('main');



// Corporate routes
    Route::get('/enterprise', [EnterpriseController::class, 'showHomeCompany'])->name('enterprise.home');
    Route::get('/seeApplicants', [EnterpriseController::class, 'showApplicants'])->name('enterprise.seeApplicants');
    Route::post('/create-company', [EnterpriseController::class, 'createCompany'])->name('create-company');
    Route::post('/create-job-offer', [EnterpriseController::class, 'createJobOffer'])->name('create-job-offer');


// Candidate routes
    Route::get('/personal', [CandidateController::class, 'showHomeCandidate'])->name('candidate.home');
    Route::post('/create-personal', [CandidateController::class, 'createPersonal'])->name('create-personal');
    Route::get('/candidate/apply/{id}', [CandidateController::class, 'applyOffer'])->name('candidate.apply');

});

